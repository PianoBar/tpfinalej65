﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSim
{
    public class RandomValor<A>
    {
        public A Valor { get; protected set; }
        public double Random { get; protected set; }
        public int Random100 { get; protected set; }
        public RandomValor(double rnd, A valor)
        {
            this.Valor = valor;
            this.Random = rnd;
            this.Random100 = (int) Math.Truncate(rnd * 100);
        }
    }
}
