﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSim
{
    public class ValorProbabilidad<B>
    {
        public B Valor { get; set; }
        public double Probabilidad { get; set; }
        public ValorProbabilidad(B valor, double probabilidad)
        {
            this.Valor = valor;
            this.Probabilidad = probabilidad;
        }
    }
}
