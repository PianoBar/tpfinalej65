﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSim
{
    public class DetalleTestChiCuadrado
    {
        public List<double> Valores { get; protected set; }
        public Intervalo Intervalo { get; protected set; }
        public int FrecuenciaObservada { get; protected set; }
        public double FrecuenciaEsperada { get; set; }
        public double FrecuenciaObservadaRel { get; set; }
        public double FrecuenciaEsperadaRel { get; set; }
        public double PruebaChiCuadrado { get; protected set; }

        public DetalleTestChiCuadrado(Intervalo inter)
        {
            this.Intervalo = inter;
        }

        public void setValores(List<double> matches)
        {
            this.Valores = matches.ToList<double>();
            this.FrecuenciaObservada = matches.Count();
        }
        public void agregarValor(double val)
        {
            this.Valores.Add(val);
            this.FrecuenciaObservada++;
        }
        public double CalcularValorPrueba()
        {
            if (this.FrecuenciaEsperada == 0)
                throw new ArgumentException("La Frecuencia Esperada no puede ser 0");
            double valor = Math.Pow(this.FrecuenciaEsperada - this.FrecuenciaObservada, 2) / this.FrecuenciaEsperada;
            this.PruebaChiCuadrado =valor;
            return valor;
        }

        public override string ToString()
        {
            return $"det: {Intervalo.ToString()} ({FrecuenciaObservada})";
        }

    }
}
