﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibSim.Generadores;

namespace LibSim.Distribuciones
{
    public class DistribucionEmpirica<T> : IDistribucion
    {
        public List<ValorProbabilidad<T>> ListaProbabilidades { get;protected set; }
        public List<ValorProbabilidad<T>> ProbabilidadesAcumuladas { get;protected set; }
        private bool usa100;
        public IGeneradorAleatorio GeneradorAleatorio { get; set; }
        
        public DistribucionEmpirica(List<ValorProbabilidad<T>> lista)
        {
            if (!this.probOK(lista))
                throw new ArgumentException("Las Probabilidades son incorrectas");
            if (usa100)
                convertirDouble(lista);
            this.ListaProbabilidades = lista;
            this.ProbabilidadesAcumuladas = hacerProbAcum(lista);
            this.AsignarGenerador(new DefaultGenerator());
        }


        private bool probOK(List<ValorProbabilidad<T>> l)
        {
            double acum = 0;
            foreach (var item in l)
            {
                acum += item.Probabilidad;
            }
            if (acum > 1)
            {
                usa100 = true;
                return acum == 100;
            }
            else
            {
                usa100 = false;
                return acum == 1;
            }

        }


        private void convertirDouble(List<ValorProbabilidad<T>> l)
        {
            foreach (var item in l)
            {
                item.Probabilidad = item.Probabilidad / 100;
            }
        }


        private List<ValorProbabilidad<T>> hacerProbAcum(List<ValorProbabilidad<T>> l)
        {
            double acum = 0;
            var listaAcum = new List<ValorProbabilidad<T>>(l.Capacity);
            foreach (var item in l)
            {
                acum += item.Probabilidad;
                listaAcum.Add(new ValorProbabilidad<T>(item.Valor, acum));
            }
            return listaAcum;
        }

        public void AsignarGenerador(IGeneradorAleatorio generador)
        {
            this.GeneradorAleatorio = generador;
        }

        public int CantidadParametros()
        {
            throw new NotImplementedException();
        }

        public bool esContinua()
        {
            return false;
        }

        public List<double> FrecuenciasEsperadas(List<Intervalo> intervalos)
        {
            throw new NotImplementedException();
        }

        public double Generar()
        {
            return this.GeneradorAleatorio.Generar();
        }


        public RandomValor<T> GenerarValor()
        {
            var rnd = this.Generar();
            foreach (var prob in this.ProbabilidadesAcumuladas)
            {
                if (rnd < prob.Probabilidad)
                    return new RandomValor<T>(rnd, prob.Valor);
            }
            return null;
        }

        public List<double> Generar(int cantidad)
        {
            List<double> l = new List<double>(cantidad);
            for (int i = 0; i < cantidad; i++)
            {
                l.Add(this.Generar());
            }
            return l;
        }

        public double getMax()
        {
            throw new NotImplementedException();
        }

        public double getMin()
        {
            throw new NotImplementedException();
        }

        RandomValor<double> IDistribucion.Generar()
        {
            return null;
        }

        public string getMensaje()
        {
            return $"Empirica segun tabla de probabilidades.";
        }
    }
}
