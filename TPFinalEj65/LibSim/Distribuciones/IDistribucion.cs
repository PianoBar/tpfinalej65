﻿using System.Collections.Generic;
using LibSim.Generadores;

namespace LibSim.Distribuciones
{
    public interface IDistribucion
    {
        void AsignarGenerador(IGeneradorAleatorio generador);
        double getMin();
        double getMax();
        RandomValor<double> Generar();
        List<double> Generar(int cantidad);
        
        List<double> FrecuenciasEsperadas(List<Intervalo> intervalos);
        int CantidadParametros();
        bool esContinua();
        string getMensaje();
    }
}
