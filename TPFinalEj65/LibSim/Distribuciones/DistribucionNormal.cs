﻿using System;
using System.Collections.Generic;
using Meta.Numerics.Statistics.Distributions;

using LibSim.Generadores;

namespace LibSim.Distribuciones
{
    public class DistribucionNormal: IDistribucion
    {
        public double Media { get; protected set; }
        public double Varianza { get; protected set; }
        public double DesviasionEstandar { get; protected set; }
        public IGeneradorAleatorio Generador { get; protected set; }
        private double min;
        private double max;

        private double? pendiente = null;

        public DistribucionNormal(double media, double varianza)
        {
            if (varianza < 0)
                throw new NotSupportedException("La varianza no puede ser negativa");

            Media = media;
            Varianza = varianza;
            Generador = new DefaultGenerator();
            min = media;
            max = 0;
        }
        public DistribucionNormal(double media, double desv_est, IGeneradorAleatorio generador)
        {
            if (desv_est < 0)
                throw new NotSupportedException("La varianza no puede ser negativa");

            DesviasionEstandar = desv_est;
            Media = media;
            Varianza = Math.Pow(desv_est, 2);
            Generador = generador;
            min = media;
            max = 0;
        }

        public void AsignarGenerador(IGeneradorAleatorio generador)
        {
            Generador = generador;
        }

        public RandomValor<double> Generar()
        {
            if(this.pendiente != null)
            {
                var x2pendiente = pendiente.Value;
                pendiente = null;
                if (x2pendiente < this.min) this.min = x2pendiente;
                if (x2pendiente > this.max) this.max = x2pendiente;
                return new RandomValor<double>(0, x2pendiente);
            }
            var aleatorio1 = Generador.Generar();
            var aleatorio2 = Generador.Generar();

            var z1 = Math.Sqrt(-2 * Math.Log(aleatorio1)) * Math.Cos(2 * Math.PI * aleatorio2);
            var z2 = Math.Sqrt(-2 * Math.Log(aleatorio1)) * Math.Sin(2 * Math.PI * aleatorio2);

            var x1 = Media + (z1 * DesviasionEstandar);
            var x2 = Media + (z2 * DesviasionEstandar);
            
            this.pendiente = x2;

            if (x1 < this.min) this.min = x1;
            if (x1 > this.max) this.max = x1;
            return new RandomValor<double>(aleatorio1, x1);
        }

        public List<double> Generar(int cantidad)
        {
            var variables = new List<double>(cantidad);

            for (int i = 0; i < cantidad; i++)
            {
                variables.Add(Generar().Valor);
            }

            return variables;
        }

        public List<double> FrecuenciasEsperadas(List<Intervalo> intervalos)
        {
            var frecuencias = new List<double>(intervalos.Count);

            Distribution d = new NormalDistribution(Media, DesviasionEstandar);

            foreach (var intervalo in intervalos)
            {
                var prob_desde = d.LeftProbability(intervalo.Desde);
                var prob_hasta = d.LeftProbability(intervalo.Hasta);
                var frecuencia =  prob_hasta - prob_desde;

                frecuencias.Add(frecuencia);
            }

            return frecuencias;
        }

        public int CantidadParametros()
        {
            return 2;
        }

        public double getMin()
        {
            return this.min;
        }

        public double getMax()
        {
            return this.max;
        }

        public bool esContinua()
        {
            return true;
        }

        public string getMensaje()
        {
            return $"Normal con media de {this.Media} y Varianza de {this.Varianza}";
        }
    }
}
