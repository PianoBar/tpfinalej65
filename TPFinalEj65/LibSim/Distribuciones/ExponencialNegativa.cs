﻿using System;
using System.Collections.Generic;
using LibSim.Generadores;
using Meta.Numerics.Statistics.Distributions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSim.Distribuciones
{
    public class ExponencialNegativa: IDistribucion
    {
        public double Lambda { get; protected set; }
        public IGeneradorAleatorio Generador { get; protected set; }
        private double max;

        public ExponencialNegativa(double lambda)
        {
            if (lambda <= 0)
                throw new NotSupportedException("Lambda debe ser un número positivo");

            Lambda = lambda;
            Generador = new DefaultGenerator();
            max = 0;
        }

        public ExponencialNegativa(double lambda, IGeneradorAleatorio generador)
        {
            if (lambda <= 0)
                throw new NotSupportedException("Lambda debe ser un número positivo");

            Lambda = lambda;
            Generador = generador;
            max = 0;
        }

        public void AsignarGenerador(IGeneradorAleatorio generador)
        {
            Generador = generador;
        }

        public RandomValor<double> Generar()
        {
            //x=-1/lambda*ln(1-r1)
            var aleatorio = Generador.Generar();

            var variable = (-1 / Lambda) * Math.Log(1 - aleatorio);
            if (variable > this.max) this.max = variable;
            return new RandomValor<double>(aleatorio, variable);
        }

        public List<double> Generar(int cantidad)
        {
            var variables = new List<double>(cantidad);

            for (int i = 0; i < cantidad; i++)
            {
                variables.Add(Generar().Valor);
            }

            return variables;
        }

        public List<double> FrecuenciasEsperadas(List<Intervalo> intervalos)
        {
            var frecuencias = new List<double>(intervalos.Count);

            //Distribution d = new ExponentialDistribution(1 / Lambda);

            foreach (var intervalo in intervalos)
            {
                //Funciona nuestra formula.
                /*var propio = DensidadAcumulada(intervalo.Desde);
                var ext = d.LeftProbability(intervalo.Desde);*/
                //var frecuencia = d.LeftProbability(intervalo.Hasta) - d.LeftProbability(intervalo.Desde);
                var frecuencia = DensidadAcumulada(intervalo.Hasta) - DensidadAcumulada(intervalo.Desde);

                frecuencias.Add(frecuencia);
            }

            return frecuencias;
        }
        private double DensidadAcumulada(double x)
        {
            return  1 - Math.Exp(-this.Lambda * x);
        }

        public int CantidadParametros()
        {
            return 1;
        }

        public double getMin()
        {
            return 0;
        }

        public double getMax()
        {
            return this.max;
        }
        public bool esContinua()
        {
            return true;
        }

        public string getMensaje()
        {
            return $"Exponencial Negativa con lambda de {this.Lambda}";
        }
    }
}
