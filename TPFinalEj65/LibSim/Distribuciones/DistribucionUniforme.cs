﻿using LibSim.Generadores;
using System.Linq;
using System;
using System.Collections.Generic;

namespace LibSim.Distribuciones
{
    public class DistribucionUniforme:IDistribucion
    {
        public double A { get; protected set; }
        public double B { get; protected set; }
        public IGeneradorAleatorio Generador { get; protected set; }

        public DistribucionUniforme(double a, double b)
        {
            if (b <= a)
                throw new NotSupportedException("A debe ser menor que B");

            A = a;
            B = b;
            Generador = new DefaultGenerator();
        }

        public DistribucionUniforme(double a, double b, IGeneradorAleatorio generador)
        {
            if (b <= a)
                throw new NotSupportedException("A debe ser menor que B");

            A = a;
            B = b;
            Generador = generador;
        }

        public void AsignarGenerador(IGeneradorAleatorio generador)
        {
            Generador = generador;
        }

        public RandomValor<double> Generar()
        {
            var aleatorio = Generador.Generar();

            var variable = A + aleatorio * (B - A);

            return new RandomValor<double>(aleatorio, variable);
        }

        public List<double> Generar(int cantidad)
        {
            var variables = new List<double>(cantidad);

            for (int i = 0; i < cantidad; i++)
            {
                variables.Add(Generar().Valor);
            }

            return variables;
        }

        public List<double> FrecuenciasEsperadas(List<Intervalo> intervalos)
        {
            var n = intervalos.Count;
            //No nos hace falta aplicar formulas de densidad acumulada si directamente los sacamos por la cant de itnervalos.
            var frecuenciaEsperada = 1 / (double)n;

            var frecuencias = new List<double>(n);

            for (int i = 0; i < n; i++)
            {
                frecuencias.Add(frecuenciaEsperada);
            }

            return frecuencias;
        }
        private double DencidadAcumulada(double x)
        {
            return (x - this.A) / (this.B - this.A);
        }
        public int CantidadParametros()
        {
            return 2;
        }

        public double getMin()
        {
            return this.A;
        }

        public double getMax()
        {
            return this.B;
        }
        public bool esContinua()
        {
            return true;
        }

        public string getMensaje()
        {
            return $"Uniforme ({this.A} , {this.B})";
        }
    }
}
