﻿using System;
using System.Collections.Generic;
using Meta.Numerics.Statistics.Distributions;
using LibSim.Generadores;

namespace LibSim.Distribuciones
{
    public class DistribucionPoisson : IDistribucion
    {

        public double Lambda { get; protected set; }
        public IGeneradorAleatorio Generador { get; protected set; }
        private int max;

        public DistribucionPoisson(double lambda)
        {
            if (lambda <= 0)
                throw new NotSupportedException("Lambda debe ser un número positivo");

            Lambda = lambda;
            Generador = new DefaultGenerator();
            max = 0;
        }
        public DistribucionPoisson(double lambda, IGeneradorAleatorio generador)
        {
            if (lambda <= 0)
                throw new NotSupportedException("Lambda debe ser un número positivo");

            Lambda = lambda;
            Generador = generador;
            max = 0;
        }

        public void AsignarGenerador(IGeneradorAleatorio generador)
        {
            this.Generador = generador;
        }

        public int CantidadParametros()
        {
            return 1;
        }

        public List<double> FrecuenciasEsperadas(List<Intervalo> intervalos)
        {
            var frecuencias = new List<double>(intervalos.Count);
           
            foreach (var intervalo in intervalos)
            {
                var frecuencia = DensidadIntervalo((int)intervalo.Desde, (int)intervalo.Hasta);
                frecuencias.Add(frecuencia);
            }

            return frecuencias;
        }

        private double DensidadIntervalo(int desde, int hasta)
        {
            int k = desde;
            double prob = 0 ;
            do
            {
                prob += Densidad(k);
                k++;
            } while (k <= hasta);
            return prob;
        }
        private double Densidad(int k)
        {
            var e = Math.Exp(-Lambda);
            var lam = Math.Pow(Lambda, k);
            var denominador = factorial(k);
            return (e * lam) / denominador;
        }
        
        private double factorial(int number)
        {
            if (number == 1 || number == 0)
                return 1;
            else
                return number * factorial(number - 1);
        }

        public RandomValor<double> Generar()
        {
            double a = Math.Exp(-Lambda);
            double b = 1;
            int i = -1;
            double rnd = 0;

            do
            {
                i++;
                rnd = this.Generador.Generar();
                b = b * rnd;
                
            } while (b >= a);
            if (i > max) this.max = i;
            return new RandomValor<double>(rnd, i);

        }

        public List<double> Generar(int cantidad)
        {
            var variables = new List<double>(cantidad);

            for (int i = 0; i < cantidad; i++)
            {
                variables.Add(Generar().Valor);
            }

            return variables;
        }

        public double getMax()
        {
            return this.max;
        }

        public double getMin()
        {
            return 0;
        }
        public bool esContinua()
        {
            return false;
        }

        public string getMensaje()
        {
            return $"Poisson con lambda de {this.Lambda}";
        }
    }
}
