﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSim
{
    public class Intervalo
    {
        public double Desde { get; set; }
        public double Hasta { get; set; }

        public Intervalo(double desde, double hasta)
        {
            if (hasta <= desde)
                throw new NotSupportedException("Intervalo incorrecto");

            Desde = desde;
            Hasta = hasta;
        }
        public Intervalo(int desde, int hasta)
        {
            if (hasta < desde)
                throw new NotSupportedException("Intervalo incorrecto");

            Desde = desde;
            Hasta = hasta;
        }

        public override string ToString()
        {
            return $"[{Desde} - {Hasta}]";
        }
    }
}
