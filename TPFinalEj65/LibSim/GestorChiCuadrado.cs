﻿using System.Linq;
using System.Collections.Generic;
using LibSim.Generadores;
using LibSim.Distribuciones;
using Meta.Numerics.Statistics.Distributions;


namespace LibSim
{
    public class GestorChiCuadrado
    {

        //public IGeneradorAleatorio generador { get; protected set; }
        public IDistribucion distribucion { get; protected set; }
        public int TamañoMuestra { get; protected set; }
        public int CantidadIntervalos { get; protected set; }
        public double Alfa { get; protected set; }
        public List<double> Valores { get; protected set; }
        public double ChiCuadradoSum { get; protected set; }

        public double ValorMaximoChiCuadrado { get; protected set; }

        public List<DetalleTestChiCuadrado> DetallesPrueba { get; protected set; }
        private int tamañoIntervalo;

        /**
         * Metodo Usado para crear y culcular la prubea de bondad de chi cuadrado.
         * Si la distribucion es contnua, dar la cantidad de intervalos.
         * Si la distribucion es discreta dar el tamaño de los intervalos.
         **/
        public GestorChiCuadrado(IDistribucion distribucion, int tamañoMuestra, int cantidad_tam_Intervalos, double incertidumbre)
        {
            this.distribucion = distribucion;
            if (distribucion == null)
                return;
            TamañoMuestra = tamañoMuestra;
            Alfa = incertidumbre;

            CalcularValores();

            if (!distribucion.esContinua())
            {
                tamañoIntervalo = cantidad_tam_Intervalos;
                CalcularDetallesDiscretos();
            }
            else
            {
                CantidadIntervalos = cantidad_tam_Intervalos;
                CalcularDetalles();
            }
            CalcularFrecuenciasEsperadas();

            ObtenerValorDeTablaChiCuadrado();
        }

        private void CalcularValores()
        {
            List<double> valores = distribucion.Generar(TamañoMuestra);
            Valores = valores;
        }

        private void CalcularDetalles()
        {
            var detalles = new List<DetalleTestChiCuadrado>(CantidadIntervalos);

            var min = distribucion.getMin();
            var max = distribucion.getMax();
            var paso = (max - min) / CantidadIntervalos;

            double inicio = min;
            double fin =  min + paso;

            for (var i = 0; i < CantidadIntervalos; i++)
            {
                var detalle = new DetalleTestChiCuadrado(new Intervalo(inicio, fin));
                detalles.Add(detalle);
                var matches = Valores.Where(valor => valor >= detalle.Intervalo.Desde && valor < detalle.Intervalo.Hasta);
                detalle.setValores(matches.ToList());

                inicio = fin;
                fin += paso;
            }
            //Ajuste para los que van hasta un max

            if (detalles[detalles.Count - 1].Intervalo.Hasta == Valores.Max())
                detalles[detalles.Count - 1].agregarValor(Valores.Max());


            this.DetallesPrueba = detalles;
        }

        private void CalcularDetallesDiscretos()
        {
            
            var detalles = new List<DetalleTestChiCuadrado>();

            int min = (int) distribucion.getMin();
            int max = (int) distribucion.getMax();
            int paso = tamañoIntervalo;

            int inicio = min;
            int fin = min + (paso-1);


            do
            {
                var detalle = new DetalleTestChiCuadrado(new Intervalo(inicio, fin));
                detalles.Add(detalle);

                var matches = Valores.Where(valor => valor >= detalle.Intervalo.Desde && valor <= detalle.Intervalo.Hasta);
                detalle.setValores(matches.ToList());

                inicio = fin + 1;
                fin += paso;
            } while (inicio <= max);
            this.CantidadIntervalos = detalles.Count;            
            this.DetallesPrueba = detalles;
        }


        private void CalcularFrecuenciasEsperadas()
        {
            double sumatoriaChiCuadrado = 0;
            var detalles = this.DetallesPrueba;
            List<double> frecEsperadasRelativas = distribucion.FrecuenciasEsperadas(detalles.Select(x => x.Intervalo).ToList());
            int j = 0;
            foreach (DetalleTestChiCuadrado detalle in detalles)
            {
                detalle.FrecuenciaEsperada = frecEsperadasRelativas[j] * TamañoMuestra;
                detalle.FrecuenciaEsperadaRel = frecEsperadasRelativas[j];

                detalle.FrecuenciaObservadaRel = (double)detalle.FrecuenciaObservada / (double)TamañoMuestra;

                sumatoriaChiCuadrado += detalle.CalcularValorPrueba();
                j++;
            }

            this.ChiCuadradoSum = sumatoriaChiCuadrado;
        }

        public void ObtenerValorDeTablaChiCuadrado()
        {
            var grados = CantidadIntervalos - 1;
            
            // En vez de usar la formula, o buscar por tabla, usamos dependencia a otro paquete.
            Distribution d = new ChiSquaredDistribution(grados);
            var valor = d.InverseRightProbability(Alfa);

            ValorMaximoChiCuadrado = valor;
        }
    }
}
