﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LibSim.Generadores
{
    public class DefaultGenerator : IGeneradorAleatorio
    {
        static Random _random;
        public DefaultGenerator()
        {
            if(_random == null)
                _random = new Random();
        }
        public double Generar()
        {
            return  _random.NextDouble();
        }

        public int Generar(int cifras)
        {
            var numero = Generar();
            return (int)(numero * Math.Pow(10, cifras));
        }
    }
}
