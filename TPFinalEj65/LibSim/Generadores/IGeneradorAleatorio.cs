﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSim.Generadores
{
    public interface IGeneradorAleatorio
    {
        double Generar();
        int Generar(int cifras);
    }
}
