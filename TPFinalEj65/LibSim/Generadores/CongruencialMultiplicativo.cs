﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSim.Generadores
{
    public class CongruencialMultiplicativo : IGeneradorAleatorio
    {
        public double Semilla { get; protected set; }
        public double A { get; protected set; }
        public double M { get; protected set; }

        public CongruencialMultiplicativo(double semilla, double a, double m)
        {
            this.Semilla = semilla;
            this.A = a;
            this.M = m;
        }
        public double Generar()
        {
            var Xi = (A * Semilla) % M;
            this.Semilla = Xi;

            var Ri = Xi / this.M;
            return Ri;
        }

        public int Generar(int cifras)
        {
            var Ri = Generar();
            return (int)(Ri * Math.Pow(10, cifras));
        }
    }
}
