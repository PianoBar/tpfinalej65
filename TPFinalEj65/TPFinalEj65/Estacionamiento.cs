﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using LibSim.Distribuciones;
using LibSim;

namespace SimulacionUTN
{
    public partial class Estacionamiento : Form
    {
        public Estacionamiento()
        {
            InitializeComponent();
        }

        private void Estacionamiento_Load(object sender, EventArgs e)
        {
            dgv_demorado.Rows.Add(10, 20);
            dgv_demorado.Rows.Add(0, 80);

            txtTiempoSimulacion.Text = "24";
            txtmostrardesde.Text = "1";
            txtCantmostrar.Text = "100";
            txtLlegada.Text = "7";
            txtCantidadLugares.Text = "5";
            txtTpoMinEst.Text = "1";
            txtTpoMaxEst.Text = "60";
            txtTpoInfraccion.Text = "5";
            txtTpoInspeccion.Text = "1";
            txtProxInspeccion.Text = "120";

        }

        private void AgregarColumnas(int coche)
        {
            var columns = new DataGridViewColumn[2];
            
            DataGridViewColumn columnEstado = new DataGridViewTextBoxColumn();
            columnEstado.CellTemplate = new DataGridViewTextBoxCell();
            columnEstado.Name = $"Estado_coche_{coche}";
            columnEstado.HeaderText = $@"Estado Coche {coche}";
            columnEstado.Width = 120;
            columnEstado.FillWeight = 1;
            columns[0] = (columnEstado);

            DataGridViewColumn columnafinestac = new DataGridViewTextBoxColumn();
            columnafinestac.CellTemplate = new DataGridViewTextBoxCell();
            columnafinestac.Name = $"Fin_Estacionamiento {coche}";
            columnafinestac.HeaderText = $@"Fin Estacionamiento {coche}";
            columnafinestac.Width = 80;
            columnafinestac.FillWeight = 1;
            columns[1] = (columnafinestac);

            dgv_simulacion.Columns.AddRange(columns);
        }

        private void simular_Click(object sender, EventArgs e)
        {
            try
            {
                var lDemorado = new List<ValorProbabilidad<int>>();
                var lInspeccion = new List<ValorProbabilidad<int>>();
                var lInfraccion = new List<ValorProbabilidad<int>>();
                var lProxInspeccion = new List<ValorProbabilidad<int>>();

                foreach (DataGridViewRow item in dgv_demorado.Rows)
                {
                    if (item.Cells[0].Value != null)
                    {
                        int tiempo = int.Parse(item.Cells[0].Value.ToString());
                        double prob = double.Parse(item.Cells[1].Value.ToString());
                        lDemorado.Add(new ValorProbabilidad<int>(tiempo, prob));
                    }
                }

                lInfraccion.Add(new ValorProbabilidad<int>(int.Parse(txtTpoInfraccion.Text), 100));
                lInspeccion.Add(new ValorProbabilidad<int>(int.Parse(txtTpoInspeccion.Text), 100));
                lProxInspeccion.Add(new ValorProbabilidad<int>(int.Parse(txtProxInspeccion.Text), 100));

                var DistDemorado = new DistribucionEmpirica<int>(lDemorado);

                var llegada = int.Parse(txtLlegada.Text);
                var DistLlegada = new ExponencialNegativa((double)1 / llegada);
                var min = int.Parse(txtTpoMinEst.Text);
                var max = int.Parse(txtTpoMaxEst.Text);
                var DistTpoEstacionamiento = new DistribucionUniforme(min, max);

                var tiempo_cobro = double.Parse(txtCantidadLugares.Text);

                Simulador manejador = new Simulador();
                manejador.DistribucionDemoraEstacionamiento = DistDemorado;
                manejador.DistribucionProximaLlegada = DistLlegada;
                manejador.DistribucionTiempoInfraccion = new DistribucionEmpirica<int>(lInfraccion);
                manejador.DistribucionTiempoInspeccion = new DistribucionEmpirica<int>(lInspeccion);
                manejador.DistribucionProximaInspeccion = new DistribucionEmpirica<int>(lProxInspeccion);
                manejador.DistribucionTiempoEstacionamiento = DistTpoEstacionamiento;

                double tiempo_simulacion = double.Parse(txtTiempoSimulacion.Text);
                tiempo_simulacion *= 60;
                int desde = int.Parse(txtmostrardesde.Text);
                int cantidad = int.Parse(txtCantmostrar.Text);

                manejador.TiempoASimular = tiempo_simulacion;
                manejador.MostrarDesde = desde;
                manejador.CantidadAMostrar = cantidad;

                //Ejecutamos la simulacion en segundo plano
                backgroundWorker1.RunWorkerAsync(manejador);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //Ejecutmaos la simulacion en segundo plano.
        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            Simulador manejador = (Simulador)e.Argument;
            manejador.Simular();
            e.Result = manejador;
        }

        // Cuando termina la simulacion. Solo cargamos los datos.
        private void backgroundWorker1_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            Simulador manejador = (Simulador)e.Result;
            this.lblCantNoIngresaron.Text = manejador.CantidadNoEstacionaron.ToString();
            this.lblTotalAutos.Text = manejador.CantidadEstacionaron.ToString();
            this.lblCantInfracciones.Text = manejador.CantidadInfraccion.ToString();
            this.txtIteracionesRealizadas.Text = manejador.IteracionesRealizadas.ToString();
            this.txtTiempoSimulado.Text = $"{Math.Round(manejador.TiempoSimulado / 60)} Hs.";

            dgv_simulacion.DataSource = manejador.Simulacion;
            dgv_simulacion.Columns[2].Frozen = true;
        }
    }
}
