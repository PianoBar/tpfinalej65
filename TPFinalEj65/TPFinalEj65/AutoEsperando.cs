﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPFinalEj65
{
    public class AutoEsperando
    {
        public EstadoAuto Estado { get; set; }
        public double TiempoEstadia { get; set; }
        public double HoraLlegada { get; set; }
        public double FinEstacionamiento { get; set; }
        public int Ubicacion { get; set; }
        public override string ToString()
        {
            return Enum.GetName(Estado.GetType(), this.Estado);
        }
    }

    public enum EstadoAuto
    {
        Estacionado,
        SiendoMultado,
        SiendoInspeccionado
    }
}
