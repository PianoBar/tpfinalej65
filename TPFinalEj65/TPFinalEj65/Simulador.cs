﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LibSim;
using System.Data;
using System.Collections;
using LibSim.Distribuciones;
using TPFinalEj65;
using TPFinalEj65.Eventos;

namespace SimulacionUTN
{
    public class Simulador
    {
        public DistribucionEmpirica<int> DistribucionDemoraEstacionamiento { get; set; }
        public DistribucionEmpirica<int> DistribucionTiempoInspeccion { get; set; }
        public DistribucionEmpirica<int> DistribucionTiempoInfraccion { get; set; }
        public DistribucionEmpirica<int> DistribucionProximaInspeccion { get; set; }
        public IDistribucion DistribucionProximaLlegada { get; set; }
        public IDistribucion DistribucionTiempoEstacionamiento { get; set; }

        public int MostrarDesde { get; set; }
        public int CantidadAMostrar { get; set; }

        /// <summary> Tiempo a simular en minutos.</summary>
        public double TiempoASimular { get; set; }

        public int IteracionesRealizadas { get; protected set; }

        /// <summary> Tiempo calculado en minutos.</summary>
        public double TiempoSimulado { get; protected set; }
        public int LugaresDisponibles { get; set; }

        public int CantidadNoEstacionaron { get; protected set; }
        public int CantidadInfraccion { get; protected set; }
        public int CantidadEstacionaron { get; protected set; }

        public List<VectorSimulacion> Simulacion { get; protected set; }


        //Para la simulacion
        private AutoEsperando[] listaAutos;
        private EstadoInspector estadoInspector;

        public Simulador()
        {
            this.LugaresDisponibles = 5;
        }

        public void Simular()
        {
            var l = new List<VectorSimulacion>();
            
            //Autos en el sistema.
            listaAutos = new AutoEsperando[this.LugaresDisponibles];

            //Hasta que fila voy a mostrar.
            int hasta = MostrarDesde + CantidadAMostrar;

            //Inicializacion del vector
            var vAnterior = new VectorSimulacion();
            vAnterior.Nro = 0;
            vAnterior.Reloj = 0;
            vAnterior.Evento = "Inicio";
            vAnterior.CantidadOcupada = 0;
            vAnterior.CantidadInfraccion = 0;
            vAnterior.AcumTotalEstacionados = 0;
            vAnterior.NoEncontraronLugar = 0;
            vAnterior.setGuardado(99);
            vAnterior.setLista(listaAutos.ToList());

            //Proxima Llegada (Primer evento)
            var proxima_llegada = DistribucionProximaLlegada.Generar();
            vAnterior.RndProxLlegada = proxima_llegada.Random100;
            vAnterior.TiempoEntreLlegada = Math.Round(proxima_llegada.Valor, 2);
            vAnterior.ProximaLlegada = vAnterior.TiempoEntreLlegada.Value;

            var proximaInspeccion = DistribucionProximaInspeccion.GenerarValor();
            vAnterior.TiempoProximaInspeccionCuadra = proximaInspeccion.Valor;
            vAnterior.ProximaInspeccionCuadra = proximaInspeccion.Valor;

            //Render zonas de cobro. De las Variables al Vector.
            vAnterior.EstadoInspector = Enum.GetName(EstadoInspector.Libre.GetType(), EstadoInspector.Libre);

            //Guardamos siempre la primera
            l.Add(vAnterior);

            do
            {
                VectorSimulacion vActual = null;

                //Obtengo el proximo evento.
                IEvento evento_actual = ProximoEvento(vAnterior);
                vActual = evento_actual.Ejecutar(vAnterior);

                //Aumento el numero de iteracion.
                vActual.Nro++;

                //Muestro el arreglo de Autos en el Vector.
                RenderizarAutos(vActual);

                // Lo guardo para mostrar o no
                if (vActual.Nro >= MostrarDesde && vActual.Nro <= hasta)
                    l.Add(vActual);

                // Chequeo que en ningun caso vuelva el tiempo para atras.
                // Puede suceder que se mantenga en el mismo instante. Pero nunca para atras.
                if (vActual.Reloj < vAnterior.Reloj)
                    System.Windows.Forms.MessageBox.Show($"El reloj volvio atras en {vActual.Reloj}");

                // Libero el vector anterior. Pongo el actual como anterior y vuelvo a iterar.
                vAnterior = vActual;


            } while (vAnterior.Reloj < this.TiempoASimular);

            //Mostramos la ultima iteracion a la fuerza.
            if (l.Last() != vAnterior)
                l.Add(vAnterior);

            /* Calculo los datos necesarios y guardo la informacion en las
             * variables pertinentes para su posterior uso. 
             */
            this.CantidadNoEstacionaron = vAnterior.NoEncontraronLugar;
            this.CantidadEstacionaron = vAnterior.AcumTotalEstacionados;
            this.CantidadInfraccion = vAnterior.CantidadInfraccion;
            this.IteracionesRealizadas = vAnterior.Nro;
            this.TiempoSimulado = vAnterior.Reloj;
            this.Simulacion = l;

        }

        /// <summary>
        /// Guardo el Arreglo actual de autos en el vector.
        /// </summary>
        /// <param name="vector">Vector en el que guardar los datos</param>
        private void RenderizarAutos(VectorSimulacion vector)
        {
            int i = 0;
            foreach (var auto in vector.getListaAutos())
            {
                string nro = i.ToString("00");

                // Tiene que coincidir con la propiedad Auto00_Estado
                string property_estado = $"Auto{nro}_Estado";
                
                // Tiene que coincidir con la propiedad Auto00_TiempoEstadia
                string property_TiempoEstadia = $"Auto{nro}_TiempoEstadia";
                
                // Tiene que coincidir con la propiedad Auto00_FinEstacionamiento
                string property_FinEstacionamiento = $"Auto{nro}_FinEstacionamiento";

                // Tiene que coincidir con la propiedad Auto00_FinEstacionamiento
                string property_HoraLlegada = $"Auto{nro}_HoraLlegada";

                // Tiene que coincidir con la propiedad Auto00_FinEstacionamiento
                string property_Ubicacion = $"Auto{nro}_Ubicacion";

                // Tiene que coincidir con la propiedad Auto00_Tipo
                string property_Tipo = $"Auto{nro}_Tipo";


                if (vector.getAuto(i) != null)
                {
                    //Magia para llamar a las propiedades de forma dinamica.
                    var estado = auto.Estado;
                    vector.GetType().GetProperty(property_estado).SetValue(vector, Enum.GetName(estado.GetType(), estado), null);
                    vector.GetType().GetProperty(property_TiempoEstadia).SetValue(vector, auto.TiempoEstadia.ToString(), null);
                    vector.GetType().GetProperty(property_HoraLlegada).SetValue(vector, auto.HoraLlegada.ToString(), null);
                    vector.GetType().GetProperty(property_Ubicacion).SetValue(vector, auto.Ubicacion.ToString(), null);
                    vector.GetType().GetProperty(property_FinEstacionamiento).SetValue(vector, auto.FinEstacionamiento.ToString(), null);
                }
                i++;
            }
        }

        /// <summary>
        /// Buscamos el proximo evento segun los tiempos en el vector actual
        /// </summary>
        /// <param name="vecActual">Vector Actual</param>
        /// <returns></returns>
        private IEvento ProximoEvento(VectorSimulacion vecActual)
        {
            //Por defecto. Proxima llegada. Ya que siempre hay proxima llegada.
            IEvento evento_actual = new EventoProximaLlegada(this.LugaresDisponibles, 
                                            (ExponencialNegativa)this.DistribucionProximaLlegada, 
                                            (DistribucionUniforme)DistribucionTiempoEstacionamiento, 
                                            this.DistribucionDemoraEstacionamiento);

            double tiempo_actual = vecActual.ProximaLlegada;

            if (vecActual.FinInspeccion.HasValue && vecActual.FinInspeccion.Value < tiempo_actual)
            {
                evento_actual = new EventoFinInspeccion(this.LugaresDisponibles, this.DistribucionProximaInspeccion, this.DistribucionTiempoInspeccion, this.DistribucionTiempoInfraccion);
                tiempo_actual = vecActual.FinInspeccion.Value;
            }

            if (vecActual.FinConfeccionMulta.HasValue && vecActual.FinConfeccionMulta.Value < tiempo_actual)
            {
                evento_actual = new EventoFinMulta(this.LugaresDisponibles, this.DistribucionProximaInspeccion, this.DistribucionTiempoInspeccion, this.DistribucionTiempoInfraccion);
                tiempo_actual = vecActual.FinConfeccionMulta.Value;
            }

            if (vecActual.ProximaInspeccionCuadra.HasValue && vecActual.ProximaInspeccionCuadra.Value < tiempo_actual)
            {
                evento_actual = new EventoProximaInspeccionCuadra(this.LugaresDisponibles, this.DistribucionProximaInspeccion, this.DistribucionTiempoInspeccion, this.DistribucionTiempoInfraccion);
                tiempo_actual = vecActual.ProximaInspeccionCuadra.Value;
            }

            //Alguno de los 20 autos termino el estacionamiento.
            var auto = vecActual.getListaAutos()
                .Where(item => item != null && (item.Estado == EstadoAuto.Estacionado || item.Estado == EstadoAuto.SiendoInspeccionado))
                .OrderBy(item => item.FinEstacionamiento)
                .FirstOrDefault();

            if (auto != null && auto.FinEstacionamiento < tiempo_actual)
            {
                evento_actual = new EventoFinEstacionamiento(auto.Ubicacion, this.LugaresDisponibles, this.DistribucionProximaInspeccion, this.DistribucionTiempoInspeccion, this.DistribucionTiempoInfraccion);
                tiempo_actual = auto.FinEstacionamiento;
            }

            //Devuelvo el vector actual
            return evento_actual;
        }

    }

    //Estados y Eventos
    public enum Evento
    {
        Inicio,
        Llegada,
        FinEstacionamiento,
        FinInspeccion,
        FinInfraccion
    }

    public enum EstadoInspector
    {
        Libre,
        Inspeccionando,
        ConfeccionBoleta
    }

}
