﻿namespace SimulacionUTN
{
    partial class Estacionamiento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgv_simulacion = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtLlegada = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtTiempoSimulado = new System.Windows.Forms.Label();
            this.txtIteracionesRealizadas = new System.Windows.Forms.Label();
            this.lblCantInfracciones = new System.Windows.Forms.Label();
            this.lblCantNoIngresaron = new System.Windows.Forms.Label();
            this.lblTotalAutos = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtCantidadLugares = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.simular = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.dgv_demorado = new System.Windows.Forms.DataGridView();
            this.txtTiempoSimulacion = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txtCantmostrar = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtmostrardesde = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtProxInspeccion = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTpoInfraccion = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtTpoInspeccion = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.txtTpoMaxEst = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtTpoMinEst = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.Tipo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProbTipo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_simulacion)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_demorado)).BeginInit();
            this.groupBox6.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv_simulacion
            // 
            this.dgv_simulacion.AllowUserToAddRows = false;
            this.dgv_simulacion.AllowUserToDeleteRows = false;
            this.dgv_simulacion.AllowUserToResizeColumns = false;
            this.dgv_simulacion.AllowUserToResizeRows = false;
            this.dgv_simulacion.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_simulacion.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv_simulacion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_simulacion.Location = new System.Drawing.Point(12, 260);
            this.dgv_simulacion.Name = "dgv_simulacion";
            this.dgv_simulacion.ReadOnly = true;
            this.dgv_simulacion.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_simulacion.Size = new System.Drawing.Size(1068, 328);
            this.dgv_simulacion.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtLlegada);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(218, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 63);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Llegada Auto";
            // 
            // txtLlegada
            // 
            this.txtLlegada.Location = new System.Drawing.Point(94, 27);
            this.txtLlegada.Name = "txtLlegada";
            this.txtLlegada.Size = new System.Drawing.Size(77, 20);
            this.txtLlegada.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(52, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Media";
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.txtTiempoSimulado);
            this.groupBox3.Controls.Add(this.txtIteracionesRealizadas);
            this.groupBox3.Controls.Add(this.lblCantInfracciones);
            this.groupBox3.Controls.Add(this.lblCantNoIngresaron);
            this.groupBox3.Controls.Add(this.lblTotalAutos);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Location = new System.Drawing.Point(866, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(214, 251);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Resultados";
            // 
            // txtTiempoSimulado
            // 
            this.txtTiempoSimulado.AutoSize = true;
            this.txtTiempoSimulado.Location = new System.Drawing.Point(155, 198);
            this.txtTiempoSimulado.Name = "txtTiempoSimulado";
            this.txtTiempoSimulado.Size = new System.Drawing.Size(10, 13);
            this.txtTiempoSimulado.TabIndex = 4;
            this.txtTiempoSimulado.Text = "-";
            // 
            // txtIteracionesRealizadas
            // 
            this.txtIteracionesRealizadas.AutoSize = true;
            this.txtIteracionesRealizadas.Location = new System.Drawing.Point(155, 222);
            this.txtIteracionesRealizadas.Name = "txtIteracionesRealizadas";
            this.txtIteracionesRealizadas.Size = new System.Drawing.Size(10, 13);
            this.txtIteracionesRealizadas.TabIndex = 4;
            this.txtIteracionesRealizadas.Text = "-";
            // 
            // lblCantInfracciones
            // 
            this.lblCantInfracciones.AutoSize = true;
            this.lblCantInfracciones.Location = new System.Drawing.Point(155, 54);
            this.lblCantInfracciones.Name = "lblCantInfracciones";
            this.lblCantInfracciones.Size = new System.Drawing.Size(10, 13);
            this.lblCantInfracciones.TabIndex = 4;
            this.lblCantInfracciones.Text = "-";
            // 
            // lblCantNoIngresaron
            // 
            this.lblCantNoIngresaron.AutoSize = true;
            this.lblCantNoIngresaron.Location = new System.Drawing.Point(155, 79);
            this.lblCantNoIngresaron.Name = "lblCantNoIngresaron";
            this.lblCantNoIngresaron.Size = new System.Drawing.Size(10, 13);
            this.lblCantNoIngresaron.TabIndex = 4;
            this.lblCantNoIngresaron.Text = "-";
            // 
            // lblTotalAutos
            // 
            this.lblTotalAutos.AutoSize = true;
            this.lblTotalAutos.Location = new System.Drawing.Point(155, 28);
            this.lblTotalAutos.Name = "lblTotalAutos";
            this.lblTotalAutos.Size = new System.Drawing.Size(10, 13);
            this.lblTotalAutos.TabIndex = 4;
            this.lblTotalAutos.Text = "-";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 28);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(119, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Cantidad Estacionados:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 79);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(140, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Cantidad que no ingresaron:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(9, 198);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(91, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "Tiempo Simulado:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 222);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(117, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "Iteraciones Realizadas:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 54);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(113, 13);
            this.label12.TabIndex = 2;
            this.label12.Text = "Cantidad Infracciones:";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtCantidadLugares);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Location = new System.Drawing.Point(218, 81);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(200, 67);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Lugares Estacionamiento";
            // 
            // txtCantidadLugares
            // 
            this.txtCantidadLugares.Location = new System.Drawing.Point(94, 22);
            this.txtCantidadLugares.Name = "txtCantidadLugares";
            this.txtCantidadLugares.Size = new System.Drawing.Size(77, 20);
            this.txtCantidadLugares.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(44, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Cantidad";
            // 
            // simular
            // 
            this.simular.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.simular.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simular.Location = new System.Drawing.Point(758, 208);
            this.simular.Name = "simular";
            this.simular.Size = new System.Drawing.Size(102, 46);
            this.simular.TabIndex = 5;
            this.simular.Text = "Simular";
            this.simular.UseVisualStyleBackColor = false;
            this.simular.Click += new System.EventHandler(this.simular_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.dgv_demorado);
            this.groupBox5.Location = new System.Drawing.Point(424, 12);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(349, 190);
            this.groupBox5.TabIndex = 6;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Demora Estacionamiento";
            // 
            // dgv_demorado
            // 
            this.dgv_demorado.AllowUserToResizeColumns = false;
            this.dgv_demorado.AllowUserToResizeRows = false;
            this.dgv_demorado.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv_demorado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_demorado.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Tipo,
            this.ProbTipo});
            this.dgv_demorado.Location = new System.Drawing.Point(6, 30);
            this.dgv_demorado.Name = "dgv_demorado";
            this.dgv_demorado.Size = new System.Drawing.Size(337, 150);
            this.dgv_demorado.TabIndex = 0;
            // 
            // txtTiempoSimulacion
            // 
            this.txtTiempoSimulacion.Location = new System.Drawing.Point(94, 19);
            this.txtTiempoSimulacion.Name = "txtTiempoSimulacion";
            this.txtTiempoSimulacion.Size = new System.Drawing.Size(77, 20);
            this.txtTiempoSimulacion.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Tiempo(Hs)";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.txtCantmostrar);
            this.groupBox6.Controls.Add(this.label8);
            this.groupBox6.Controls.Add(this.txtmostrardesde);
            this.groupBox6.Controls.Add(this.label7);
            this.groupBox6.Controls.Add(this.txtTiempoSimulacion);
            this.groupBox6.Controls.Add(this.label1);
            this.groupBox6.Location = new System.Drawing.Point(12, 12);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(200, 136);
            this.groupBox6.TabIndex = 8;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Tiempo";
            // 
            // txtCantmostrar
            // 
            this.txtCantmostrar.Location = new System.Drawing.Point(94, 77);
            this.txtCantmostrar.Name = "txtCantmostrar";
            this.txtCantmostrar.Size = new System.Drawing.Size(77, 20);
            this.txtCantmostrar.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 79);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Cant. a Mostrar";
            // 
            // txtmostrardesde
            // 
            this.txtmostrardesde.Location = new System.Drawing.Point(94, 49);
            this.txtmostrardesde.Name = "txtmostrardesde";
            this.txtmostrardesde.Size = new System.Drawing.Size(77, 20);
            this.txtmostrardesde.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 52);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Mostrar Desde";
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtProxInspeccion);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtTpoInfraccion);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.txtTpoInspeccion);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Location = new System.Drawing.Point(12, 154);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 100);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Inspector";
            // 
            // txtProxInspeccion
            // 
            this.txtProxInspeccion.Location = new System.Drawing.Point(136, 74);
            this.txtProxInspeccion.Name = "txtProxInspeccion";
            this.txtProxInspeccion.Size = new System.Drawing.Size(58, 20);
            this.txtProxInspeccion.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(124, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Proxima Inspeccion (min)";
            // 
            // txtTpoInfraccion
            // 
            this.txtTpoInfraccion.Location = new System.Drawing.Point(136, 48);
            this.txtTpoInfraccion.Name = "txtTpoInfraccion";
            this.txtTpoInfraccion.Size = new System.Drawing.Size(58, 20);
            this.txtTpoInfraccion.TabIndex = 5;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 51);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(117, 13);
            this.label13.TabIndex = 4;
            this.label13.Text = "Tiempo Infraccion (min)";
            // 
            // txtTpoInspeccion
            // 
            this.txtTpoInspeccion.Location = new System.Drawing.Point(136, 22);
            this.txtTpoInspeccion.Name = "txtTpoInspeccion";
            this.txtTpoInspeccion.Size = new System.Drawing.Size(58, 20);
            this.txtTpoInspeccion.TabIndex = 3;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 25);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(122, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Tiempo Inspeccion (min)";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.txtTpoMaxEst);
            this.groupBox7.Controls.Add(this.label15);
            this.groupBox7.Controls.Add(this.txtTpoMinEst);
            this.groupBox7.Controls.Add(this.label14);
            this.groupBox7.Location = new System.Drawing.Point(218, 154);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(200, 100);
            this.groupBox7.TabIndex = 5;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Tiempo Estacionamiento";
            // 
            // txtTpoMaxEst
            // 
            this.txtTpoMaxEst.Location = new System.Drawing.Point(94, 48);
            this.txtTpoMaxEst.Name = "txtTpoMaxEst";
            this.txtTpoMaxEst.Size = new System.Drawing.Size(77, 20);
            this.txtTpoMaxEst.TabIndex = 5;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(20, 51);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(68, 13);
            this.label15.TabIndex = 4;
            this.label15.Text = "Maximo (min)";
            // 
            // txtTpoMinEst
            // 
            this.txtTpoMinEst.Location = new System.Drawing.Point(94, 22);
            this.txtTpoMinEst.Name = "txtTpoMinEst";
            this.txtTpoMinEst.Size = new System.Drawing.Size(77, 20);
            this.txtTpoMinEst.TabIndex = 3;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(23, 25);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(65, 13);
            this.label14.TabIndex = 1;
            this.label14.Text = "Minimo (min)";
            // 
            // Tipo
            // 
            this.Tipo.HeaderText = "Tiempo Demora";
            this.Tipo.Name = "Tipo";
            this.Tipo.Width = 98;
            // 
            // ProbTipo
            // 
            this.ProbTipo.HeaderText = "Probabilidad";
            this.ProbTipo.Name = "ProbTipo";
            this.ProbTipo.Width = 90;
            // 
            // Estacionamiento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1092, 600);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.simular);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.dgv_simulacion);
            this.Name = "Estacionamiento";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Estacionamiento Medido";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Estacionamiento_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_simulacion)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_demorado)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv_simulacion;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button simular;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.DataGridView dgv_demorado;
        private System.Windows.Forms.TextBox txtLlegada;
        private System.Windows.Forms.TextBox txtCantidadLugares;
        private System.Windows.Forms.TextBox txtTiempoSimulacion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox txtCantmostrar;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtmostrardesde;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblCantNoIngresaron;
        private System.Windows.Forms.Label lblTotalAutos;
        private System.Windows.Forms.Label txtIteracionesRealizadas;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label txtTiempoSimulado;
        private System.Windows.Forms.Label label11;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Label lblCantInfracciones;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtTpoInfraccion;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtTpoInspeccion;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox txtTpoMaxEst;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtTpoMinEst;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtProxInspeccion;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tipo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProbTipo;
    }
}