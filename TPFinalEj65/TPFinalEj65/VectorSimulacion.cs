﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPFinalEj65
{
    public class VectorSimulacion
    {
        public int Nro { get; set; }
        public string Evento { get; set; }
        public double Reloj { get; set; }

        /*----------LLEGADA AUTO---------------*/
        public double? RndProxLlegada { get; set; }
        public double? TiempoEntreLlegada { get; set; }
        public double ProximaLlegada { get; set; }
        /*----------LLEGADA AUTO---------------*/

        /*-------FIN ESTACIONAMIENTO-----------*/
        public int? RndEstadia { get; set; }
        public double? Estadia { get; set; }
        public int? RndDemora { get; set; }
        public int? TiempoDemora { get; set; }
        public double? FinEstacionamiento { get; set; }
        /*-------FIN ESTACIONAMIENTO-----------*/

        /*--------------INSPECTOR--------------*/
        public string EstadoInspector { get; set; }
        public double? TiempoInspeccion { get; set; }
        public double? FinInspeccion { get; set; }
        public double? TiempoMulta { get; set; }
        public double? FinConfeccionMulta { get; set; }
        public int? LugarInspeccionando { get; set; }
        public int? TiempoProximaInspeccionCuadra { get; set; }
        public double? ProximaInspeccionCuadra { get; set; }
        /*--------------INSPECTOR--------------*/

        public int CantidadOcupada { get; set; }
        public int NoEncontraronLugar { get; set; }
        public int CantidadInfraccion { get; set; }
        public int AcumTotalEstacionados { get; set; }

        public string Auto00_Estado { get; set; }
        public string Auto00_TiempoEstadia { get; set; }
        public string Auto00_FinEstacionamiento { get; set; }
        public string Auto00_HoraLlegada { get; set; }
        public string Auto00_Ubicacion { get; set; }

        public string Auto01_Estado { get; set; }
        public string Auto01_TiempoEstadia { get; set; }
        public string Auto01_FinEstacionamiento { get; set; }
        public string Auto01_HoraLlegada { get; set; }
        public string Auto01_Ubicacion { get; set; }

        public string Auto02_Estado { get; set; }
        public string Auto02_TiempoEstadia { get; set; }
        public string Auto02_FinEstacionamiento { get; set; }
        public string Auto02_HoraLlegada { get; set; }
        public string Auto02_Ubicacion { get; set; }

        public string Auto03_Estado { get; set; }
        public string Auto03_TiempoEstadia { get; set; }
        public string Auto03_FinEstacionamiento { get; set; }
        public string Auto03_HoraLlegada { get; set; }
        public string Auto03_Ubicacion { get; set; }

        public string Auto04_Estado { get; set; }
        public string Auto04_TiempoEstadia { get; set; }
        public string Auto04_FinEstacionamiento { get; set; }
        public string Auto04_HoraLlegada { get; set; }
        public string Auto04_Ubicacion { get; set; }

        //Donde queda guardado el proximo auto.
        private int Guardado;
        public void setGuardado(int l)
        {
            this.Guardado = l;
        }
        public int getGuardado()
        {
            return this.Guardado;
        }

        private List<AutoEsperando> ListaAutos;
        public void setAuto(int ubicacion, AutoEsperando auto)
        {
            this.ListaAutos[ubicacion] = auto;
        }
        public AutoEsperando getAuto(int ubicacion)
        {
            return this.ListaAutos[ubicacion];
        }
        public List<AutoEsperando> getListaAutos()
        {
            return this.ListaAutos;
        }
        public void setLista(List<AutoEsperando> lista)
        {
            this.ListaAutos = lista;
        }

        public VectorSimulacion Copy()
        {
            var copia = new VectorSimulacion();
            copia.Nro = this.Nro;
            copia.ProximaLlegada = this.ProximaLlegada;

            copia.CantidadOcupada = this.CantidadOcupada;
            copia.NoEncontraronLugar = this.NoEncontraronLugar;
            copia.CantidadInfraccion = this.CantidadInfraccion;
            copia.AcumTotalEstacionados = this.AcumTotalEstacionados;

            copia.Guardado = this.Guardado;
            copia.ListaAutos = this.ListaAutos;

            copia.EstadoInspector = this.EstadoInspector;
            copia.FinInspeccion = this.FinInspeccion;
            copia.FinConfeccionMulta = this.FinConfeccionMulta;
            copia.LugarInspeccionando = this.LugarInspeccionando;
            copia.ProximaInspeccionCuadra = this.ProximaInspeccionCuadra;

            return copia;
        }

    }
}
