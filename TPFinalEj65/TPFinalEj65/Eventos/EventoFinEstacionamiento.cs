﻿using LibSim.Distribuciones;
using SimulacionUTN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPFinalEj65.Eventos
{
    public class EventoFinEstacionamiento : BaseEventoInspeccion, IEvento
    {
        //Posicion del auto que esta por salir
        public int UbicacionFin { get; set; }

        public EventoFinEstacionamiento(int ubicacion, int lugaresDisponibles, DistribucionEmpirica<int> distrProximaInspeccionCuadra, DistribucionEmpirica<int> distrTpoInspeccion, DistribucionEmpirica<int> distrTpoInfraccion)
        {
            this.UbicacionFin = ubicacion;
            this.LugaresDisponibles = lugaresDisponibles;

            this.DistrProximaInspeccionCuadra = distrProximaInspeccionCuadra;
            this.DistrTpoInfraccion = distrTpoInfraccion;
            this.DistrTpoInspeccion = distrTpoInspeccion;
        }

        public VectorSimulacion Ejecutar(VectorSimulacion vAnterior)
        {
            var vActual = vAnterior.Copy();
            vActual.Evento = "Fin Estacionamiento";

            var auto = vActual.getAuto(this.UbicacionFin);

            vActual.Reloj = auto.FinEstacionamiento;

            if (auto.Estado == EstadoAuto.SiendoInspeccionado)
            {
                //Se cancela el evento de Inspeccion Auto

                this.MoverSiguienteUbicacion(vActual);
            }
            
            //El auto sale del sistema
            vActual.setAuto(this.UbicacionFin, null);
            vActual.CantidadOcupada--;
            vActual.AcumTotalEstacionados++;

            return vActual;
        }
    }
}
