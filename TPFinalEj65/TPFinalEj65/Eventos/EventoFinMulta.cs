﻿using LibSim.Distribuciones;
using SimulacionUTN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPFinalEj65.Eventos
{
    public class EventoFinMulta : BaseEventoInspeccion, IEvento
    {
        public EventoFinMulta(int lugaresDisponibles, DistribucionEmpirica<int> distrProximaInspeccionCuadra, DistribucionEmpirica<int> distrTpoInspeccion, DistribucionEmpirica<int> distrTpoInfraccion)
        {
            this.LugaresDisponibles = lugaresDisponibles;

            this.DistrProximaInspeccionCuadra = distrProximaInspeccionCuadra;
            this.DistrTpoInfraccion = distrTpoInfraccion;
            this.DistrTpoInspeccion = distrTpoInspeccion;
        }

        public VectorSimulacion Ejecutar(VectorSimulacion vAnterior)
        {
            var vActual = vAnterior.Copy();
            vActual.Evento = "Fin Confeccion Multa";
            vActual.Reloj = vAnterior.FinConfeccionMulta.Value;
            var auto = vActual.getAuto(vActual.LugarInspeccionando.Value);

            vActual.CantidadInfraccion++;
            auto.Estado = EstadoAuto.Estacionado;
            vActual.setAuto(vActual.LugarInspeccionando.Value, auto);

            MoverSiguienteUbicacion(vActual);

            return vActual;
        }
    }
}
