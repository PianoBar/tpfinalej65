﻿using LibSim.Distribuciones;
using SimulacionUTN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPFinalEj65.Eventos
{
    public class EventoProximaInspeccionCuadra : BaseEventoInspeccion, IEvento
    {
        public EventoProximaInspeccionCuadra(int lugaresDisponibles, DistribucionEmpirica<int> distrProximaInspeccionCuadra, DistribucionEmpirica<int> distrTpoInspeccion, DistribucionEmpirica<int> distrTpoInfraccion)
        {
            this.LugaresDisponibles = lugaresDisponibles;
            this.DistrProximaInspeccionCuadra = distrProximaInspeccionCuadra;
            this.DistrTpoInfraccion = distrTpoInfraccion;
            this.DistrTpoInspeccion = distrTpoInspeccion;
        }

        public VectorSimulacion Ejecutar(VectorSimulacion vAnterior)
        {
            var vActual = vAnterior.Copy();
            vActual.Evento = "Inspeccion Cuadra";

            vActual.Reloj = vActual.ProximaInspeccionCuadra.Value;
            vActual.ProximaInspeccionCuadra = null;
            vActual.LugarInspeccionando = -1;

            /*El inspector se mueve a la proxima ubicacion o genera la proxima inspeccion de cuadra*/
            MoverSiguienteUbicacion(vActual);

            return vActual;
        }
    }
}
