﻿using LibSim.Distribuciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPFinalEj65.Eventos
{
    public interface IEvento
    {
        VectorSimulacion Ejecutar(VectorSimulacion vAnterior);
    }
}
