﻿using LibSim.Distribuciones;
using SimulacionUTN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPFinalEj65.Eventos
{
    public class BaseEventoInspeccion
    {
        public int LugaresDisponibles { get; set; }

        public DistribucionEmpirica<int> DistrProximaInspeccionCuadra { get; set; }
        public DistribucionEmpirica<int> DistrTpoInspeccion { get; set; }
        public DistribucionEmpirica<int> DistrTpoInfraccion { get; set; }

        public void MoverSiguienteUbicacion(VectorSimulacion vActual)
        {
            AutoEsperando auto = null;

            do
            {
                //Si no hay ningun auto en esa ubicacion se mueve a la siguiente
                vActual.LugarInspeccionando++;
                if (vActual.LugarInspeccionando.Value < this.LugaresDisponibles)
                {
                    auto = vActual.getAuto(vActual.LugarInspeccionando.Value);
                }
            } while (auto == null && vActual.LugarInspeccionando.Value != this.LugaresDisponibles);

            vActual.FinConfeccionMulta = null;

            if (vActual.LugarInspeccionando.Value == this.LugaresDisponibles)
            {
                // No tiene mas lugares para inspeccionar
                vActual.EstadoInspector = Enum.GetName(EstadoInspector.Libre.GetType(), EstadoInspector.Libre);
                var proxima = this.DistrProximaInspeccionCuadra.GenerarValor();
                vActual.TiempoProximaInspeccionCuadra = proxima.Valor;
                vActual.ProximaInspeccionCuadra = proxima.Valor + vActual.Reloj;
                vActual.FinInspeccion = null;
                vActual.LugarInspeccionando = null;
            }
            else
            {
                var inspeccion = this.DistrTpoInspeccion.GenerarValor();

                auto.Estado = EstadoAuto.SiendoInspeccionado;
                vActual.EstadoInspector = Enum.GetName(EstadoInspector.Inspeccionando.GetType(), EstadoInspector.Inspeccionando);
                vActual.TiempoInspeccion = inspeccion.Valor;
                vActual.FinInspeccion = inspeccion.Valor + vActual.Reloj;

                vActual.setAuto(vActual.LugarInspeccionando.Value, auto);
            }
        }
    }
}
