﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibSim.Distribuciones;

namespace TPFinalEj65.Eventos
{
    public class EventoProximaLlegada : IEvento
    {
        public int LugaresDisponibles { get; set; }

        public ExponencialNegativa DistribucionProxLlegada { get; set; }
        public DistribucionUniforme DistribucionEstacionamiento { get; set; }
        public DistribucionEmpirica<int> DistrDemora { get; set; }

        public EventoProximaLlegada(int lugaresDisponibles, ExponencialNegativa distribucionProxLlegada, DistribucionUniforme distribucionEstacionamiento, DistribucionEmpirica<int> distrDemora)
        {
            this.LugaresDisponibles = lugaresDisponibles;

            this.DistrDemora = distrDemora;
            this.DistribucionEstacionamiento = distribucionEstacionamiento;
            this.DistribucionProxLlegada = distribucionProxLlegada;
        }

        public VectorSimulacion Ejecutar(VectorSimulacion vAnterior)
        {
            //Realizo copia del vector actual. Por los acumuladores.
            var vActual = vAnterior.Copy();
            vActual.Evento = "Llegada Auto";
            vActual.Reloj = vAnterior.ProximaLlegada;

            //Calculo la proxima llegada.
            var proxima_llegada = this.DistribucionProxLlegada.Generar();
            vActual.RndProxLlegada = proxima_llegada.Random100;
            vActual.TiempoEntreLlegada = Math.Round(proxima_llegada.Valor, 2);
            vActual.ProximaLlegada = Math.Round(vActual.TiempoEntreLlegada.Value + vActual.Reloj, 2);

            //Hay lugares para estacionar.
            if (vAnterior.CantidadOcupada == this.LugaresDisponibles)
            {
                //Lo cuento
                vActual.NoEncontraronLugar++;
            }
            else
            {
                //Lo Cuento
                vActual.CantidadOcupada++;

                //Calculo el tiempo de estadia.
                var estadia = DistribucionEstacionamiento.Generar();
                vActual.RndEstadia = estadia.Random100;
                vActual.Estadia = Math.Round(estadia.Valor, 2);
                var demora = DistrDemora.GenerarValor();
                vActual.RndDemora = demora.Random100;
                vActual.TiempoDemora = demora.Valor;
                vActual.FinEstacionamiento = vActual.Reloj + estadia.Valor + demora.Valor;

                //Vemos si el proximo esta ocupado o no.
                //Y reiteramos hasta que encontremos un lugar vacio.
                var lista = vActual.getListaAutos();
                var guardado = vActual.getGuardado();

                do
                {

                    if (guardado >= lista.Capacity - 1)
                        guardado = 0;
                    else
                        guardado++;

                } while (lista[guardado] != null);

                AutoEsperando auto = new AutoEsperando();
                auto.Estado = EstadoAuto.Estacionado;
                auto.TiempoEstadia = Math.Round(estadia.Valor, 2);
                auto.HoraLlegada = vActual.Reloj;
                auto.Ubicacion = guardado;
                auto.FinEstacionamiento = Math.Round(vActual.FinEstacionamiento.Value, 2);

                //Guardo el fin de estacionamiento y el auto recien llegado
                vActual.FinEstacionamiento = auto.FinEstacionamiento;
                vActual.setAuto(guardado, auto);
            }

            return vActual;
        }
    }
}
