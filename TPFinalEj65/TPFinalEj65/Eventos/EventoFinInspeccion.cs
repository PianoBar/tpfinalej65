﻿using LibSim.Distribuciones;
using SimulacionUTN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPFinalEj65.Eventos
{
    public class EventoFinInspeccion : BaseEventoInspeccion, IEvento
    {
        public EventoFinInspeccion(int lugaresDisponibles, DistribucionEmpirica<int> distrProximaInspeccionCuadra, DistribucionEmpirica<int> distrTpoInspeccion, DistribucionEmpirica<int> distrTpoInfraccion)
        {
            this.LugaresDisponibles = lugaresDisponibles;

            this.DistrProximaInspeccionCuadra = distrProximaInspeccionCuadra;
            this.DistrTpoInfraccion = distrTpoInfraccion;
            this.DistrTpoInspeccion = distrTpoInspeccion;
        }

        public VectorSimulacion Ejecutar(VectorSimulacion vAnterior)
        {
            var vActual = vAnterior.Copy();
            vActual.Evento = "Fin Inspeccion";
            vActual.Reloj = vAnterior.FinInspeccion.Value;
            var auto = vActual.getAuto(vActual.LugarInspeccionando.Value);

            /*Actualizo el estado del auto*/
            auto.Estado = EstadoAuto.Estacionado;
            if (vActual.Reloj > auto.TiempoEstadia + auto.HoraLlegada)
            {
                var infraccion = this.DistrTpoInfraccion.GenerarValor();
                //esta demorado
                auto.Estado = EstadoAuto.SiendoMultado;
                vActual.EstadoInspector = Enum.GetName(EstadoInspector.ConfeccionBoleta.GetType(), EstadoInspector.ConfeccionBoleta);
                vActual.TiempoMulta = infraccion.Valor;
                vActual.FinConfeccionMulta = infraccion.Valor + vActual.Reloj;
                vActual.FinInspeccion = null;
                //Si el auto termina su estacionamiento antes de finalizar la multa actulizo el fin estacionamiento
                if (vActual.FinConfeccionMulta > auto.FinEstacionamiento)
                {
                    auto.FinEstacionamiento = vActual.FinConfeccionMulta.Value;
                }

                vActual.setAuto(vActual.LugarInspeccionando.Value, auto);
            }
            else
            {
                /*Si no tiene que hacer una multa el inspector se mueve a la proxima ubicacion o genera la proxima inspeccion de cuadra*/
                vActual.setAuto(vActual.LugarInspeccionando.Value, auto);
                MoverSiguienteUbicacion(vActual);
            }

            return vActual;
        }
    }
}
